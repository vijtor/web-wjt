!function () {
    function e(e) {
        e = e || window.event, e.preventDefault && e.preventDefault(), e.returnValue = !1
    }

    function n(n) {
        for (var o = w.length; o--;) if (n.keyCode === w[o]) return void e(n)
    }

    function o(n) {
        e(n)
    }

    function t(e) {
    }

    function i() {
        window.onmousewheel = document.onmousewheel = t, document.onkeydown = n, document.body.ontouchmove = o
    }

    function l() {
        window.onmousewheel = document.onmousewheel = document.onkeydown = document.body.ontouchmove = null
    }

    function s() {
        return window.pageYOffset || p.scrollTop
    }

    function r() {
        if (a = s(), u && !f) {
            if (0 > a) return !1;
            window.scrollTo(0, 0)
        }
        return classie.has(v, "notrans") ? (classie.remove(v, "notrans"), !1) : m ? !1 : void(0 >= a && c ? d(0) : a > 0 && !c && d(1))
    }

    function d(e) {
        m = !0, e ? classie.add(v, "modify") : (u = !0, i(), classie.remove(v, "modify")), setTimeout(function () {
            c = !c, m = !1, e && (u = !1, l())
        }, 600)
    }

    $(window).load(function () {
        $("#preloader").delay(1e3).fadeOut("slow")
    });
    var a, c, u, m, f = function () {
            var e, n = -1, o = window.navigator.userAgent, t = o.indexOf("MSIE "), i = o.indexOf("Trident/");
            if (t > 0) n = parseInt(o.substring(t + 5, o.indexOf(".", t)), 10); else if (i > 0) {
                var l = o.indexOf("rv:");
                n = parseInt(o.substring(l + 3, o.indexOf(".", l)), 10)
            }
            return n > -1 ? n : e
        }(), w = [32, 37, 38, 39, 40], p = window.document.documentElement, v = document.getElementById("cross-portfolio"),
        y = v.querySelector("button.trigger"), g = s();
    u = 0 === g, i(), g && (c = !0, classie.add(v, "notrans"), classie.add(v, "modify")), window.addEventListener("scroll", r), y.addEventListener("click", function () {
        d("reveal")
    }), $(".element").typed({
        strings: ["I'm Web Developer", "I'm Web Designer"],
        typeSpeed: 1,
        backSpeed: 1,
        backDelay: 1e3,
        loop: !0
    }), $(".sub-title").typed({
        strings: ["Web Developer", "Web Designer"],
        typeSpeed: 1,
        backSpeed: 1,
        backDelay: 1400,
        loop: !0
    }), $("#nav-icon").click(function () {
        $(this).toggleClass("open"), $("#menu-overlay").toggleClass("menu-show")
    }), $(".smooth-scroll").click(function () {
        $("#nav-icon").removeClass("open"), $("#menu-overlay").removeClass("menu-show")
    });
    new Waypoint({
        element: document.getElementById("count"), handler: function () {
            $(".count").countTo()
        }, offset: 500
    });
    smoothScroll.init({speed: 1e3}), $(".carousel-inner").owlCarousel({
        navigation: !1,
        slideSpeed: 300,
        paginationSpeed: 400,
        singleItem: !0,
        autoPlay: 3e3
    })
    //     window.sr = ScrollReveal().reveal(".animated"), $(".gmap").mobileGmap({
    //     markers: [{
    //         position: "center",
    //         info: "121 S Pinckney St",
    //         showInfo: !0
    //     }]
    // })
}();




// 百度地图API功能
var map = new BMap.Map('map');
map.addControl(new BMap.NavigationControl());//缩放平移控件
map.addControl(new BMap.ScaleControl());    //比例尺
map.addControl(new BMap.OverviewMapControl());//缩略图
map.addControl(new BMap.MapTypeControl()); //地图类型
// map.setCurrentCity("青岛");
var poi = new BMap.Point(116.403847,39.915561);
map.centerAndZoom(poi,16);
map.enableScrollWheelZoom();


var content = '<div style="margin:0;line-height:20px;padding:2px;">' +
                'Tel：176 0011 6574<br/>Email：vijtor@163.com' +
                '</div>';

// 创建检索信息窗口对象
var searchInfoWindow = null;
searchInfoWindow = new BMapLib.SearchInfoWindow(map, content, {
    title: "未九涛",      //标题
    width: 290,             //宽度
    height: 50,              //高度
    panel: "panel",         //检索结果面板
    // enableAutoPan: true,     //自动平移
    // enableSendToPhone : true, //是否启动发送到手机功能
    searchTypes: [
        BMAPLIB_TAB_SEARCH,   //周边检索
        BMAPLIB_TAB_TO_HERE,  //到这里去
        BMAPLIB_TAB_FROM_HERE //从这里出发
    ]
});
var marker = new BMap.Marker(poi); //创建marker对象
marker.enableDragging(); //marker可拖拽
marker.addEventListener("click", function (e) {
    searchInfoWindow.open(marker);
})
map.addOverlay(marker); //在地图中添加marker






$(function () {
;
    mouseClick();
    mouseClickHeart();

});




//华丽的分割线


var a_idx = 0;
var a = new Array("富强", "民主", "文明", "和谐", "自由", "平等", "公正", "法治", "爱国", "敬业", "诚信", "友善");

function mouseClick() {
    $("body").click(function (e) {
        a_idx = (a_idx + 1) % a.length;
        var x = e.pageX,
            y = e.pageY;
        var $i = $("<span/>").text(a[a_idx]);
        $i.css({
            "z-index": 999,
            "top": y - 20,
            "left": x,
            "position": "absolute",
            "font-weight": "bold",
            "color": "#ff6651"
        });
        $("body").append($i);
        $i.animate({
                "top": y - 180,
                "opacity": 0
            },
            1500,
            function () {
                $i.remove();
            });
    });


}



//鼠标点击出心
var hearts = [];
function mouseClickHeart(e) {
    window.requestAnimationFrame = (function () {
        return window.requestAnimationFrame ||
            window.webkitRequestAnimationFrame ||
            window.mozRequestAnimationFrame ||
            window.oRequestAnimationFrame ||
            window.msRequestAnimationFrame ||
            function (callback) {
                setTimeout(callback, 1000 / 60);
            }
    })();
    init();
    function init() {
        css(".heart{width: 10px;height: 10px;position: fixed;background: #f00;transform: rotate(45deg);-webkit-transform: rotate(45deg);-moz-transform: rotate(45deg);}.heart:after,.heart:before{content: '';width: inherit;height: inherit;background: inherit;border-radius: 50%;-webkit-border-radius: 50%;-moz-border-radius: 50%;position: absolute;}.heart:after{top: -5px;}.heart:before{left: -5px;}");
        attachEvent();
        gameloop();
    }

    function gameloop() {
        for (var i = 0; i < hearts.length; i++) {
            if (hearts[i].alpha <= 0) {
                document.body.removeChild(hearts[i].el);
                hearts.splice(i, 1);
                continue;
            }
            hearts[i].y--;
            hearts[i].scale += 0.004;
            hearts[i].alpha -= 0.013;
            hearts[i].el.style.cssText = "left:" + hearts[i].x + "px;top:" + hearts[i].y + "px;opacity:" + hearts[i].alpha + ";transform:scale(" + hearts[i].scale + "," + hearts[i].scale + ") rotate(45deg);background:" + hearts[i].color;
        }
        requestAnimationFrame(gameloop);
    }

    function attachEvent() {
        var old = typeof window.onclick === "function" && window.onclick;
        window.onclick = function (event) {
            old && old();
            createHeart(event);
        }
    }

    function createHeart(event) {
        var d = document.createElement("div");
        d.className = "heart";
        hearts.push({
            el: d,
            x: event.clientX - 5,
            y: event.clientY - 5,
            scale: 1,
            alpha: 1,
            color: randomColor()
        });
        document.body.appendChild(d);
    }

    function css(css) {
        var style = document.createElement("style");
        style.type = "text/css";
        try {
            style.appendChild(document.createTextNode(css));
        } catch (ex) {
            style.styleSheet.cssText = css;
        }
        document.getElementsByTagName('head')[0].appendChild(style);
    }

    function randomColor() {
        return "rgb(" + (~~(Math.random() * 255)) + "," + (~~(Math.random() * 255)) + "," + (~~(Math.random() * 255)) + ")";
    }
}










